package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * @author Ritwik Banerjee
 * @author Nefertiti Samba
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private boolean        hardWord;    // Nef - whether or not the word is hard enough for a hint
    private boolean        usedHint;    // Nef - whether or not the button has been used
    private int            remainingGuesses;
    public  AppTemplate    appTemplate;

    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;
            this.targetWord = setTargetWord();
            this.goodGuesses = new HashSet<>();
            this.badGuesses = new HashSet<>();
            this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
            this.hardWord = checkIfHard(this.targetWord);
        } else {
            this.appTemplate = appTemplate;
        }
    }

    public void init() {
        this.targetWord = setTargetWord();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        this.hardWord = checkIfHard(this.targetWord);
    }

    @Override
    public void reset() {
        this.targetWord = null;
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        this.hardWord = false;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getTargetWord() {
        return targetWord;
    }

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;
        int[] ascii = new int[26];
        for (int i = 97, y = 0; y < ascii.length; i++, y++){
            ascii[y] = i;
        }
        int accepted = 0;

        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            String word = lines.skip(toSkip).findFirst().get();
            for (int i = 0; i < word.length(); i++){
                for (int y = 0; i < ascii.length; y++){
                    if (ascii[y] == word.charAt(i)){
                        accepted +=1;
                        break;
                    }
                    else
                        continue;
                }
            }
            if (accepted != word.length()){
                word = setTargetWord();
                return word;
            }
            else
                return word;

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public GameData setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        return this;
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    public GameData setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    public GameData setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void addGoodGuess(char c) {
        goodGuesses.add(c);
    }

    public void addBadGuess(char c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }

    public void hintGuess(){
        remainingGuesses--;
    }

    public void setUsedHint(boolean answer){
        usedHint = answer;
    }

    public boolean getUsedHint(){return usedHint;}

    public void setHardWord(boolean answer){
        hardWord = answer;
    }

    public boolean getHardWord(){return hardWord;}

    public boolean checkIfHard(String word){
        List<Character> found = new ArrayList<Character>();
        for (int i = 0; i < word.length();i++){
            if (!found.contains(word.charAt(i))) {
                found.add(word.charAt(i));
            }
        }
        if (found.size() > 7)
            return true;
        else
            return false;
    }


}
