package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.text.*;
import javafx.scene.text.Font;
import propertymanager.PropertyManager;
import settings.AppPropertyType;
import ui.AppGUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static hangman.HangmanProperties.*;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 * @author Nefertiti Samba
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    GridPane          alreadyGuessed;    // Nef- The grid of letters that shows which  letters have been guessed
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    ArrayList<Image>  hangmanDude;
    HangmanController controller;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        figurePane.setStyle("-fx-background-color: transparent;");
        figurePane.setCenter(makeHangman());
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        alreadyGuessed = new GridPane();
        gameTextsPane = new VBox();

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    //To get the guessed letters
    public HBox getGuessedLettersBox(){
        return guessedLetters;
    }

    public Button getStartGame() {
        return startGame;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        alreadyGuessed = new GridPane();
        figurePane = new BorderPane();
        figurePane.setCenter(makeHangman());
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, alreadyGuessed);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }

    public Canvas makeHangman(){
        int x = 675;
        int y = 690;
        hangmanDude = new ArrayList<Image>();
        for (int i = 9,z = 0;i >= 0;i--, z++){
            Image pic = new Image("/images/Hangman" + i + ".png");
            hangmanDude.add(z, pic);
        }
        Canvas picture = new Canvas(x,y);


        return picture;
    }

    public void updatePicture(int i) {
        ImageView pic = new ImageView(hangmanDude.get(i));
        pic.setFitHeight(690);
        pic.setFitWidth(675);
        figurePane.setCenter(pic);
    }


    public void makeAlphabet(GridPane grid){
        grid.setMinSize(200,200);

        grid.setPadding(new Insets(10,10,10,10));
        //grid.setHgap(10);
        //grid.setVgap(10);



        for (int x = 0, i = 97; x < 4; x++) {
            for (int y = 0; y < 7 && i <= 122; y++) {
                    BorderPane container = new BorderPane();
                    container.setMinSize(40,40);
                    Label letter = new Label(String.valueOf(Character.toUpperCase((char) i)));
                    container.setCenter(letter);
                    container.setStyle("-fx-border-color: black");
                    grid.add(container, y, x);
                    i++;
            }
        }

    }

    public void colorChange(boolean good, char guess){
        int rawNum = ((int)guess) - 97;
        int col = rawNum%7;
        int row = rawNum/7;
        if(good == true){
            BorderPane colored = new BorderPane();
            Label letter = new Label(String.valueOf(Character.toUpperCase(guess)));
            colored.setCenter(letter);
            colored.setStyle("-fx-background-color: green; -fx-border-color: black");
            alreadyGuessed.add(colored,col,row);
        }
        else{
            BorderPane colored = new BorderPane();
            Label letter = new Label(String.valueOf(Character.toUpperCase(guess)));
            colored.setCenter(letter);
            colored.setStyle("-fx-background-color: red; -fx-border-color: black");
            alreadyGuessed.add(colored,col,row);
        }
    }

    public void makeTargetWord(Text[] guessed, HBox container){
        TilePane target = new TilePane();
        target.setPrefColumns(guessed.length);
        target.setPrefTileWidth(40);
        target.setPrefTileHeight(40);
        target.setHgap(2);
        target.setPadding(new Insets(5,5,5,5));
        //target.setStyle("-fx-border-color: black; -fx-background-color: red");
        for (int i = 0; i < guessed.length;i++) {
            guessed[i].setFont(Font.font("Arial", 20));
            BorderPane box = new BorderPane();
            box.setCenter(guessed[i]);
            box.setStyle("-fx-border-color: black");
            target.getChildren().add(box);
        }
        container.getChildren().add(target);
    }

    public void endReveal(HBox container, Text[] guessed){
        TilePane pane = (TilePane) container.getChildren().get(0);
        for (int i = 0; i < guessed.length; i++){
            if (!(guessed[i].isVisible())){
                pane.getChildren().get(i).setStyle("-fx-background-color: gray");
                guessed[i].setVisible(true);
            }
        }
    }

    public void startStyle(){
        figurePane.setStyle("-fx-background-color: white");
    }

}
